const Knex = require('knex');
const { Datastore } = require('@google-cloud/datastore');
const fs = require('fs');
const datastore = new Datastore({
    projectId: 'e-commerce-283612'
});
const keyName = 5634161670881280;
const kind = 'database';
const stringKey = datastore.key([kind, keyName]);
var dbUser = '';
var dbPasswd = '';
var dbConnection = '';
var dbName = '';

const retrieve = async () => {
    const [entity] = await datastore.get(stringKey);
    dbUser = entity.DB_USER;
    dbPasswd = entity.DB_PASSWD;
    dbConnection = entity.DB_CONNECTION;
    dbName = entity.DB_NAME;
}

const connectWithTcp = (config) => {
    // Extract host and port from socket address
    const dbSocketAddr = process.env.DB_HOST.split(":") // e.g. '127.0.0.1:5432'
    // Establish a connection to the database
    return Knex({
        client: "pg",
        connection: {
            user: dbUser, // e.g. 'my-user'
            password: dbPasswd, // e.g. 'my-user-password'
            database: dbName, // e.g. 'my-database'
            host: '127.0.0.1', // e.g. '127.0.0.1'
            port: '3306', // e.g. '5432'
            /*ssl : {
                ca: fs.readFileSync(process.env.CERT + '/server-ca.pem'),
                cert: fs.readFileSync(process.env.CERT + '/client-cert.pem'),
                key: fs.readFileSync(process.env.CERT + '/client-key.pem'),
            },*/
        },
        // ... Specify additional properties here.
        ...config
    });
}
// [END cloud_sql_postgres_knex_create_tcp]

// [START cloud_sql_postgres_knex_create_socket]
const connectWithUnixSockets = (config) => {
    const dbSocketPath = process.env.DB_SOCKET_PATH || "/cloudsql"

    // Establish a connection to the database
    return Knex({
        client: 'pg',
        connection: {
            user: dbUser, // e.g. 'my-user'
            password: dbPasswd, // e.g. 'my-user-password'
            database: dbName, // e.g. 'my-database'
            host: `${dbSocketPath}/${dbConnection}`,
        },
        // ... Specify additional properties here.
        ...config
    });
}

const asyncConnect = async () => {
    let config = { pool: {} };
    config.pool.max = 5;
    config.pool.min = 5;
    config.pool.acquireTimeoutMillis = 60000; // 60 seconds
    config.createTimeoutMillis = 30000; // 30 seconds
    config.idleTimeoutMillis = 600000; // 10 minutes
    config.createRetryIntervalMillis = 200; // 0.2 seconds
    let knex;
    await retrieve();
    if (process.env.DB_HOST) {
        knex = connectWithTcp(config);
    } else {
        knex = connectWithUnixSockets(config);
    }
    return knex;
}

module.exports = asyncConnect();