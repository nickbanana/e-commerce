import { Component, OnInit } from '@angular/core';
import { Product } from '../shared/model/product';
import { ProductService } from '../shared/service/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-section',
  templateUrl: './product-section.component.html',
  styleUrls: ['./product-section.component.css']
})
export class ProductSectionComponent implements OnInit {

  products$: Observable<Product[]>;
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.products$ = this.productService.getProducts();
  }

}
