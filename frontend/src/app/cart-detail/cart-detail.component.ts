import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Order } from './../shared/model/order';
import { OrderDetail } from './../shared/model/orderDetail';
import { OrderState } from './../shared/reducer/order.reducer';
import { ProductService } from './../shared/service/product.service';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {
  orderDetail$: Observable<OrderDetail[]>;
  constructor(
    private store: Store<{orderlist: OrderState}>,
    private productService: ProductService
    ) {
    this.orderDetail$ = this.store
      .pipe(
        select('orderlist'),
        map(state => state.orders),
        map(orders => {
          return orders
            .map(order => this.fetchProductDetail(order));
        })
      );
  }

  ngOnInit(): void {
  }

  fetchProductDetail(order: Order): OrderDetail {
    return {
      count: order.count,
      orderId: order.orderId,
      product: this.productService.getProduct(order.productId)
    };
  }
}
