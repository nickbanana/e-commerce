import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrderState } from '../shared/reducer/order.reducer';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  orderCount$: Observable<number>;
  constructor(private store: Store<{orderlist: OrderState}>) {
    this.orderCount$ = this.store
      .pipe(
        select('orderlist'),
        map(x => x.productCount)
      );
  }

  ngOnInit(): void {
  }

}
