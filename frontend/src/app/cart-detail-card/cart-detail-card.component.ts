import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { removeOrder } from './../shared/action/order';
import { Order } from './../shared/model/order';
import { OrderDetail } from './../shared/model/orderDetail';

@Component({
  selector: 'app-cart-detail-card',
  templateUrl: './cart-detail-card.component.html',
  styleUrls: ['./cart-detail-card.component.css'],
})
export class CartDetailCardComponent implements OnInit {
  @Input() order: OrderDetail;
  constructor(
    private store: Store<Order>
  ) {}

  ngOnInit(): void {
  }
  removeOrder(): void {
    this.store.dispatch(
      removeOrder({orderId: this.order.orderId})
    );
  }
}
