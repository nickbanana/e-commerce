import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartDetailCardComponent } from './cart-detail-card.component';

describe('CartDetailCardComponent', () => {
  let component: CartDetailCardComponent;
  let fixture: ComponentFixture<CartDetailCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartDetailCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartDetailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
