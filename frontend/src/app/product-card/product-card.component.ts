import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { addOrder } from '../shared/action/order';
import { Order } from '../shared/model/order';
import { Product } from '../shared/model/product';


@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  amountArray: number[];
  selectedAmount: number;
  constructor(private store: Store<Order>) {
    this.amountArray = Array.from(Array(10), (_, x) => x + 1);
  }

  ngOnInit(): void {
  }

  addToCart(): void {
    if (!isNaN(this.selectedAmount)) {
      this.store.dispatch(
        addOrder({
          productId: this.product.id,
          count: this.selectedAmount,
          orderId: 0
        })
      );
    }
  }

}
