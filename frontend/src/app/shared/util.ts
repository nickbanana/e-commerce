import { Order } from './model/order';

export const orderCountRemover = (orders: Order[], prevCount: number, orderId: number): number => {
  return prevCount - orders.filter(order => order.orderId === orderId)[0].count;
};

export const orderRemover = (orders: Order[], orderId: number): Order[] => {
  return orders.filter(order => order.orderId !== orderId);
};

