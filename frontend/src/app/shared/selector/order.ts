import { OrderState } from '../reducer/order.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const productCountSelector = createSelector((state: OrderState) => state, state => state.productCount);

