import { Action, createReducer, on } from '@ngrx/store';
import * as OrderActions from '../action/order';
import { Order } from '../model/order';
import { orderRemover, orderCountRemover } from '../util';

export interface OrderState {
  orders: Order[];
  productCount: number;
  orderCount: number;
}

export const initialState: OrderState = {
  orders: [],
  productCount: 0,
  orderCount: 0
};

export const orderReducer = createReducer(
  initialState,
  on(OrderActions.addOrder, ( state, order ) => ({
    ...state,
    orderCount: state.orderCount + 1,
    productCount: state.productCount + order.count,
    orders: [
      ...state.orders, {...order, orderId: state.orderCount}
    ]
  })),
  on(OrderActions.removeOrder, ( state, { orderId } ) => ({
    ...state,
    orderCount: state.orderCount - 1,
    productCount: orderCountRemover(state.orders, state.productCount, orderId),
    orders: [
      ...orderRemover(state.orders, orderId)
    ]
  }))
);
