import { createAction, props } from '@ngrx/store';
import { Order } from '../model/order';

export const addOrder = createAction(
  'Add Order',
  props<Order>()
);

export const removeOrder = createAction(
  'Remove Order',
  props<{orderId: number}>()
);
