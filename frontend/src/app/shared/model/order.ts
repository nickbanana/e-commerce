export interface Order {
  count: number;
  productId: string;
  orderId: number;
}
