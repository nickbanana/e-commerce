import { Observable } from 'rxjs';
import { Product } from './product';
export interface OrderDetail {
  count: number;
  orderId: number;
  product: Observable<Product>;
}
