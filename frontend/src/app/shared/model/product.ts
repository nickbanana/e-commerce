export interface Product {
  id: string;
  index: number;
  price: number;
  picture: string;
  stockcount: number;
  name: string;
  about: string;
  categories: [string];
}
