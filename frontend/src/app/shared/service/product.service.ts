import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../model/product';
@Injectable({
  providedIn: 'root',
})
export class ProductService {
  products: Product[];

  constructor(private http: HttpClient) {
    this.getProducts().subscribe();
  }

  public getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>('/api');
  }

  // tslint:disable-next-line:variable-name
  public getProduct(id: string): Observable<Product> {
    return this.getProducts().pipe(
      map(array => array.find(obj => obj.id === id))
    );
  }
}
