import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CartDetailComponent } from './cart-detail/cart-detail.component';
import { ProductSectionComponent } from './product-section/product-section.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
  {path: 'cart', component: CartDetailComponent},
  {path: 'products', component: ProductSectionComponent},
  {path: 'product/:id', component: ProductDetailComponent},
  {path: '', redirectTo: '/products', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
