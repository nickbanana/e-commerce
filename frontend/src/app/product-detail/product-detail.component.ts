import { Component, OnInit } from '@angular/core';
import { Product } from '../shared/model/product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../shared/service/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product$: Observable<Product>;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.product$ = this.productService.getProduct(id);
  }

}
